// ProjektALG.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#include <cstdio>
#include "Maze.h"
#include "Generator.h"
bool parse_int(std::string string, int& result)
{
	int number = 0;
	int tmp = 0;
	int multiplier = 1;
	if (string.empty())
	{
		return false;
	}
	for (int i = string.length() - 1; i >= 0; i--)
	{
		if (string[i] < 48 || string[i] > 57)
		{
			return false;
		}
		//std::cout << "Test : " << number + (string[i] - 48) * multiplier << std::endl;
		if (number + (string[i] - 48) * multiplier < number)
		{
			return false;
		}
		tmp = (string[i] - 48) * multiplier;
		if ((string[i] - 48) != 0 && tmp / (string[i] - 48) != multiplier)
		{
			return false;
		}
		tmp = number + (string[i] - 48) * multiplier;
		if (tmp - (string[i] - 48) * multiplier != number)
		{
			return false;
		}
		number = number + (string[i] - 48) * multiplier;
		multiplier = multiplier * 10;
	}
	result = number;
	return true;
}
void Generate_Sample()
{
	std::fstream MyFile("../Maze.txt", std::ios::out);
	MyFile << "17" << std::endl;
	MyFile << "17" << std::endl;
	std::string pattern[17] = {
		"XXXXXXXXXXXXXXXXX",
		"S000000000000000X",
		"XXX0XXXXXXX0XX0XX",
		"X000X00000X00000X",
		"X0XXX0XXX0XXX0XXX",
		"X00000X000X00000X",
		"XXXXX0XXX0XXXXX0X",
		"X0000000X0000000X",
		"X0XXXXXXX0X0XXX0X",
		"X000X00000X00000X",
		"X0XXXXXXX0X0XXX0X",
		"X000000000000000X",
		"X0XXX0X0X0XXX0XXX",
		"X0X00000X0000X00X",
		"X0X0XXX0X0XXX0X0X",
		"X000X00000000000E",
		"XXXXXXXXXXXXXXXXX"
	};
	for (int i = 0; i < 17; i++) {
		MyFile << pattern[i] << std::endl;
	}

	MyFile.close();
}
enum State 
{
	Start,
	About,
	PreGenerated,
	GenerateNew,
	MazeView,
	SolveGenerated
};
int main()
{
	State currentState = Start;
	Maze maze;
	Generator g;
	std::string input;
	bool maze_saved = false;
	bool maze_solved = false;
	std::string test_input;
	Generate_Sample();
	while (true)
	{
		switch (currentState)
		{

		case Start:
			system("cls");
			std::cout << "Welcome to my Maze Generator/Solver" << std::endl;
			std::cout << "What would you like to do?" << std::endl;
			std::cout << "(1) About me " << std::endl;
			std::cout << "(2) Solve a premade maze " << std::endl;
			std::cout << "(3) Generate a maze " << std::endl;
			std::cout << "(4) Solve a generated maze " << std::endl;
			std::cout << "(5) Exit " << std::endl;

			char userInput;
			std::cin >> userInput;
			switch (userInput)
			{
			case '1':
				currentState = About;
				break;
			case '2':
				currentState = PreGenerated;
				break;
			case '3':
				currentState = GenerateNew;
				break;
			case '4':
				currentState = SolveGenerated;
				break;
			case '5':
				system("cls");
				std::cout << "Thanks for using my maze solver :] " << std::endl;
				return 0;
				break;
			default:
				currentState = Start;
				break;
			}
			break;

		case About:
			system("cls");
			std::cout << "Creator : Alexandr Kania" << std::endl;
			std::cout << "Mazes solved using a sort-of depth-first search algorithm I made during the second semester" << std::endl;
			std::cout << "Mazes generated using a Maze generation algorithm based on wikipedia" << std::endl;
			std::cout << "https://en.wikipedia.org/wiki/Maze_generation_algorithm" << std::endl;
			std::cout << "I used the recursive implementation but to create some dead ends I used a stack to backtrack " << std::endl;
			std::cout << "If you have any questions, contact me at kan0242@vsb.cz " << std::endl;
			std::cout << "(1) Main Menu " << std::endl;
			std::cout << "(2) Exit " << std::endl;
			std::cin >> userInput;
			switch (userInput)
			{
			case '1':
				currentState = Start;
				break;
			case '2':
				system("cls");
				std::cout << "Thanks for using my maze solver :] " << std::endl;
				return 0;
				break;
			default:
				break;
			}
			break;
		case PreGenerated:
			system("cls");
			maze.LoadMaze("../Maze.txt");
			maze.SolveMaze();
			system("cls");
			maze.PrintMaze();
			std::cout << "(1) Main Menu " << std::endl;
			std::cout << "(2) Exit " << std::endl;
			std::cin >> userInput;
			switch (userInput)
			{
			case '1':
				currentState = Start;
				break;
			case '2':
				system("cls");
				std::cout << "Thanks for using my maze solver :] " << std::endl;
				return 0;
				break;
			default:
				break;
			}
			break;


		case GenerateNew:
			system("cls");
			maze_saved = false;
			std::cout << "Let's generate a maze!" << std::endl;
			std::cout << "Please select a maze size (5 to 31)" << std::endl;
			test_input;
			int size;
			while (1)
			{
				std::cin >> test_input;
				if (!parse_int(test_input, size))
					std::cout << "Not a reasonable size" << std::endl;
				else if (size < 5 || size > 31)
					std::cout << "Not a reasonable size" << std::endl;
				else
					break;
			}	
			g = Generator(size, size);
			currentState = MazeView;
			break;


		case MazeView:
			system("cls");
			g.PrintMaze();
			if (maze_saved)
			{
				std::cout << "Maze saved succesfully" << std::endl;
			}
			std::cout << "(1) Save Maze" << std::endl;
			std::cout << "(2) Main menu without saving" << std::endl;
			std::cout << "(3) Exit without saving" << std::endl;
			std::cout << "(4) Generate again" << std::endl;
			std::cin >> userInput;
			switch (userInput)
			{
			case '1':
				g.SaveMaze();
				maze_saved = true;
				maze_solved = false;
				break;
			case '2':
				currentState = Start;
				break;
			case '3':
				system("cls");
				std::cout << "Thanks for using my maze solver :] " << std::endl;
				return 0;
				break;
			case '4':
				currentState = GenerateNew;
				break;
			default:
				break;
			}
			break;
			case SolveGenerated:
				if (maze_saved)
				{
					system("cls");
					maze.LoadMaze("../generated.txt");
					maze.SolveMaze();
					system("cls");
					maze.PrintMaze();
				}
				else
				{
					std::cout << "No maze was saved. " << std::endl;
				}
			std::cout << "(1) Main Menu " << std::endl;
			std::cout << "(2) Exit " << std::endl;
			std::cin >> userInput;
			switch (userInput)
			{
			case '1':
				currentState = Start;
				break;
			case '2':
				system("cls");
				std::cout << "Thanks for using my maze solver :] " << std::endl;
				return 0;
				break;
			default:
				break;
			}
			break;
		}
	}

	//Generator g = Generator(17, 17);
	//g.SaveMaze();


}


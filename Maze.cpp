#include "Maze.h"

Maze::Maze()
{  
    this->height = 0;
    this->length = 0;
}

Maze::~Maze()
{
    _mazeVector.clear();
}


void Maze::LoadMaze(const char* inputFile)
{
    this->height = 0;
    this->length = 0;
    this->_mazeVector.clear();
    std::string line;
    std::fstream file(inputFile);
    if (!file.is_open())
    {
        std::printf("Not found\n");
        return;
    }
    file >> line;
    this->height = std::stoi(line);
    file >> line;
    this->length = std::stoi(line);
    std::string* maze = new std::string[this->height];
    for (size_t i = 0; i < this->height; i++)
    {
        file >> line;
        maze[i] = line;
    }
    for (size_t i = 0; i <this->height; i++)
    {
        for (size_t j = 0; j < this->length; j++)
        {
            Point* point = new Point(maze[i][j]); 
            if (point->GetValue() == 'S')
                starting_position = i * length + j;
            this->_mazeVector.push_back(point);
        }
    }
    delete[] maze;
    file.close();

    //Setting up perimeters
    for (size_t i = 0; i <this->height; i++)
    {
        for (size_t j = 0; j <this->length-1; j++)
        {
            this->_mazeVector[(i * this->length) + j]->SetRight(this->_mazeVector[(i * this->length) + j + 1]);
        }
    }
    for (size_t i = 0; i <this->height; i++)
    {
        for (size_t j = 1; j <this->length; j++)
        {
            this->_mazeVector[(i * this->length) + j]->SetLeft(this->_mazeVector[(i * this->length) + j - 1]);
        }
    }
    for (size_t i = 0; i <this-> height-1; i++)
    {
        for (size_t j = 0; j <this-> length; j++)
        {
            this->_mazeVector[(i * this->length) + j]->SetBot(this->_mazeVector[((i+1) * this->length) + j]);
        }
    }
    for (size_t i = 1; i <this-> height; i++)
    {
        for (size_t j = 0; j < this-> length; j++)
        {
            this->_mazeVector[(i * this->length) + j]->SetTop(this->_mazeVector[((i-1) * this->length) + j]);
        }
    }
}

void Maze::PrintMaze()
{
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    for (size_t i = 0; i < this->height; i++)
    {
        for (size_t j = 0; j < this->length; j++)
        {
            char out = this->_mazeVector[(i * this->length) + j]->GetValue();
            if (out == 'X')
                SetConsoleTextAttribute(hConsole, 12);
            if(out == '+')
                SetConsoleTextAttribute(hConsole, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
            if (out == '*')
                SetConsoleTextAttribute(hConsole, 10);
            if (out == 'S' || out == 'E')
                SetConsoleTextAttribute(hConsole, 14);

            std::cout << this->_mazeVector[(i * this->length) + j]->GetValue();
            SetConsoleTextAttribute(hConsole, 7);
        }
        printf("\n");
    }
}

void Maze::SolveMaze()
{
    Point* current = this->_mazeVector[this->starting_position + 1];
    current->SetParent(_mazeVector[this->starting_position]);
    while (1)
    {
        system("cls");
        current->Visited();
        current->SetValue('+');
        this->PrintMaze();
        Sleep(125);
        if (current->GetRight()->GetValue() == 'E' || current->GetBot()->GetValue() == 'E' || current->GetLeft()->GetValue() == 'E' || current->GetTop()->GetValue() == 'E')
        {
            break;
        }
        if (current->GetRight() != NULL && current->GetRight()->GetValue() == '0' && current->GetRight()->wasVisited() == false)
        {
            current->GetRight()->SetParent(current);
            current = current->GetRight();
            continue;
        }
        if (current->GetBot() != NULL && current->GetBot()->GetValue() == '0' && current->GetBot()->wasVisited() == false)
        {
                current->GetBot()->SetParent(current);
                current = current->GetBot();
                continue;
        }
        if (current->GetLeft() != NULL && current->GetLeft()->GetValue() == '0' && current->GetLeft()->wasVisited() == false)
        {
                current->GetLeft()->SetParent(current);
                current = current->GetLeft();
                continue;
        }
        if (current->GetTop() != NULL && current->GetTop()->GetValue() == '0' && current->GetTop()->wasVisited() == false)
        {
                current->GetTop()->SetParent(current);
                current = current->GetTop();
                continue;
        }
        current = current->GetParent();
    }
    while (current->GetValue()!='S')
    {
        current->SetValue('*');
        current = current->GetParent();
    }
}

Point::Point(char value)
{
    this->value = value;
    this->visited = false;
    this->_right = NULL;
    this->_left = NULL;
    this->_bot = NULL;
    this->_top = NULL;
    this->_parent = NULL;
}

Point::~Point()
{
}

char Point::GetValue()
{
    return this->value;
}

void Point::SetValue(char value)
{
    this->value = value;
}

bool Point::wasVisited()
{
    return this->visited;
}

void Point::Visited()
{
    this->visited = true;
}

Point* Point::GetRight()
{
    return this->_right;
}

void Point::SetRight(Point* right)
{
    this->_right = right;
}

Point* Point::GetLeft()
{
    return this->_left;
}

void Point::SetLeft(Point* left)
{
    this->_left = left;
}

Point* Point::GetTop()
{
    return this->_top;
}

void Point::SetTop(Point* top)
{
    this->_top = top;
}

Point* Point::GetBot()
{
    return this->_bot;
}

void Point::SetBot(Point* bot)
{
    this->_bot = bot;
}

Point* Point::GetParent()
{
    return this->_parent;
}

void Point::SetParent(Point* parent)
{
    this->_parent = parent;
}

This program probably won't work on Linux, please use Windows to compile and execute it. 
This is mainly due to how I chose to implement coloring and animations.

My code is a Maze Generator / Solver.
It is a basic console interface implemented using a state machine.
Users can select from several options by writing a number and pressing enter.

One of the options is to solve a premade maze. The maze sample is saved to a .txt file upon the start of the program.
The maze is then solved with a Depth-First Search algorithm, which the user can see happen in real time.
Afterwards the user can choose to go back to the main menu or exit the program.

Another option is to generate a new maze, this is done by a recursive maze generation algorithm, with iterative backtracking added to create some dead ends
within the maze.
The user can save the maze to a .txt file.

Lastly the user can choose to solve the maze that was generated, this option won't allow the user to pick it unless he has previously saved a generated maze.



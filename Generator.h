#pragma once
#include <vector>
#include <string>
#include <random>
#include <iostream>
#include <windows.h>
#include <stack>
#include <fstream>
struct GenPoint 
{
    bool visited;
    bool wall;
    bool start;
    bool end;
    GenPoint() 
    {
        visited = false;
        wall = true;
        start = false;
        end = false;
    }
};

class Generator
{
public:
    int rows;
    int cols;
    std::stack<std::pair<int, int>> backtracking_stack;
    std::vector<std::vector<GenPoint>> maze;
    Generator()
    {

    }
    Generator(int rows,int cols)
    {
        this->rows = rows;
        this->cols = cols;
        this->maze = std::vector<std::vector<GenPoint>>(rows, std::vector<GenPoint>(cols)); 
        this->generate_maze(1, 1);
        this->maze[1][0].start = true;
        maze[rows-2][cols-1].end = true;
        system("cls");
        this->PrintMaze();
    }
   
    void generate_maze(int row, int col)
    {
        maze[row][col].visited = true;
        maze[row][col].wall = false;
        std::vector<std::pair<int, int>> unvisited_neighbors;
        if (row > 1 && !maze[row - 2][col].visited) 
        {
            unvisited_neighbors.push_back(std::make_pair(row - 2, col));
        }

        if (row < maze.size() - 2 && !maze[row + 2][col].visited)
        {
            unvisited_neighbors.push_back(std::make_pair(row + 2, col));
        }

        if (col > 1 && !maze[row][col - 2].visited) 
        {
            unvisited_neighbors.push_back(std::make_pair(row, col - 2));
        }

        if (col < maze[0].size() - 2 && !maze[row][col + 2].visited) 
        {
            unvisited_neighbors.push_back(std::make_pair(row, col + 2));
        }
        system("cls");
        this->PrintMaze();
        Sleep(125);
        if (!unvisited_neighbors.empty())
        {

            std::random_device rd;
            std::mt19937 gen(rd());
            std::uniform_int_distribution<> dis(0, unvisited_neighbors.size() - 1);

            int rand_index = dis(gen);
            int rand_row = unvisited_neighbors[rand_index].first;
            int rand_col = unvisited_neighbors[rand_index].second;

            if (rand_row < row) 
                maze[row - 1][col].wall = false;
            if (rand_row > row)
                maze[row + 1][col].wall = false;
            if (rand_col < col) 
                maze[row][col - 1].wall = false;
            if (rand_col > col)
                maze[row][col + 1].wall = false;
            backtracking_stack.push(std::make_pair(row, col));

            maze[rand_row][rand_col].wall = false;
            std::uniform_int_distribution<> dis2(0, 3);
            int chance = dis2(gen);
            if (chance == 0  || chance == 1)
            {
                auto curr = backtracking_stack.top();
                backtracking_stack.pop();
                if (chance == 1 && !backtracking_stack.empty())
                    curr = backtracking_stack.top();
                this->generate_maze(curr.first, curr.second);
            }
            
            this->generate_maze(rand_row, rand_col);

        }
    }
 
    void PrintMaze()
    {
        HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
        for (size_t i = 0; i < this->rows; i++)
        {
            for (size_t j = 0; j < this->cols; j++)
            {
                if (maze[i][j].start)
                {
                SetConsoleTextAttribute(hConsole, 14);
                std::cout << 'S';
                }
                else if (maze[i][j].end)
                {
                SetConsoleTextAttribute(hConsole, 14);
                std::cout << 'E';
                }
                else if (maze[i][j].wall)
                {
                    SetConsoleTextAttribute(hConsole, 12);
                    std::cout << 'X';
                }
                else
                    std::cout << '0';

                SetConsoleTextAttribute(hConsole, 7);
               
            }
            std::cout << std::endl;
        }
    }
    void SaveMaze()
    {
        std::fstream MyFile("../generated.txt", std::ios::out);
        MyFile << rows << std::endl;
        MyFile << cols << std::endl;
        for (size_t i = 0; i < this->rows; i++)
        {
            for (size_t j = 0; j < this->cols; j++)
            {
                if (maze[i][j].start)
                {
                    MyFile << 'S';
                }
                else if (maze[i][j].end)
                {
                    MyFile << 'E';
                }
                else if (maze[i][j].wall)
                {

                    MyFile << 'X';
                }
                else
                    MyFile << '0';


            }
            MyFile << std::endl;
        }
        MyFile.close();
    }
};
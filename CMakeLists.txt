cmake_minimum_required(VERSION 3.8)
project(tasks01)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ")

add_compile_options(-Wall  -pedantic)

# Use main.cpp to call and debug your functions
add_executable(main main.cpp Maze.cpp)

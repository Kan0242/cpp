#pragma once
#include <sstream>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <windows.h>
#include <cstdlib>

class Point
{
public:
	Point(char value);
	~Point();
	char GetValue();
	void SetValue(char value);
	bool wasVisited();
	void Visited();

	Point* GetRight();
	void  SetRight(Point* right);
	Point* GetLeft();
	void  SetLeft(Point* left);
	Point* GetTop();
	void  SetTop(Point* top);
	Point* GetBot();
	void  SetBot(Point* bot);
	Point* GetParent();
	void  SetParent(Point* parent);

private:
	char value;
	bool visited;
	Point* _right;
	Point* _left;
	Point* _top;
	Point* _bot;
	Point* _parent;
};
class Maze
{
public:
	Maze();
	~Maze();
	void LoadMaze(const char* inputFile);
	void PrintMaze();
	void SolveMaze();
private:
	int length;
	int height;
	int starting_position;
	std::vector<Point*> _mazeVector;
};
